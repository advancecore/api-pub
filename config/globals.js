module.exports = {
  environment : "sandbox",
  currency : "usd",
  bcrypt: {
    salt_round: 10,
    salt: "secret"
  },
  url: {
    user: "http://0.0.0.0:9000",
    seller: "http://0.0.0.0:9005",
    admin: "http://0.0.0.0:9002"
  },
  site_title: "Act E-Commerce",
  mail: {
    mandrill: 'TPTy95GMdPhZwdBSbpbtog'
  },
  admin_email: "portal@advancecore.com.sa"
}
